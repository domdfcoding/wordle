===============
:mod:`wordle`
===============

.. automodule:: wordle
	:skip-members: get_tokens
